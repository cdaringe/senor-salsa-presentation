# practical docker serving tasty salsa.

[senorsalsa.org](www.senorsalsa.org) is finally _a thing_.  i've put it off for years, but finally focused and knocked it out.  it's a place for me to share reviews and metrics about the world's best salsas.  this post, however, is not about salsa.  what i care to focus on is how i built and delivered this multi-service application.  perhaps i'll add a little ...spice :dancer: in, just for fun. how about it?

the following covers my practical application development & deployment, by means of example soup to nuts, with docker.  also included are bits on the web application toolchains used in the process.  i will annotate those sections as such, such that if you are only interested in the docker components, you can feel free to skip!

## salsa blog 101

how does one make a salsa blog?  [wordpress](https://wordpress.com/)?  nah.  [ghost](https://ghost.org/)?  maybe.  why reinvent the wheel vs. build something from scratch?  a rew reasons:

- senorsalsa is more than just free-formed blog  posts.  they are structured reviews, auto-composed from micro-reviews on aspects of each salsa.
- senorsalsa is more than just reviews. data i/o for the reviewed salsa is also critical.
- senorsalsa isn't off the shelf.  just as the best salsas tend to be craft salsas, so shall the greatest salsa blog be representative of expert craftsman quality.

## the stack

- native webapp (or UI, you could call it!):
  - view layer - [react](https://facebook.github.io/react/)
  - model layer (kinda) -  [redux](https://github.com/reactjs/redux)

these together boil down to some undefined MVC (Model-View-Controller) variant.  Some people have [opinions](https://www.infoq.com/articles/no-more-mvc-frameworks) on the stack, but until webcompents & HTTP2 are a thing, IMHO its one of the web design scaffolding options out there.

- DB
  - [rethinkdb](https://www.rethinkdb.com/).  the greatest.

## the ui

## the db

## the containers

## the network

## the deployment

- docker cleanup
- docker build w/ tags
- docker save/load. discuss stdin vs .gz. discuss export import (doesnt save meta)
  - discuss if there's a better deployment strategy (e.g. docker-cloud)
  - host your own registry https://docs.docker.com/registry/
  - requires encryption, so tried out letsencrypt. https://certbot.eff.org/#ubuntutrusty-other, used standalone
  - generates `.pem` files.  ~have to `chown -R user:group /path/to/pems`. use `-R` because letsencrypt symlinks these files.~
    - give access to the web user only for my certs.
    - it would appear that `REGISTRY_HTTP_TLS_CERTIFICATE` ==> `cert.pem`, however, then you receive a `Get https://cdaringe.com:5000/v1/_ping: x509: certificate signed by unknown authority`
      - thus, `fullchain.pem` bundles the `cert.pem` & `chain.pem`.
```
docker stop registry && docker rm registry
docker run -d -p 5000:5000 --restart=always --name registry \
  -v /www/docker-registry:/var/lib/registry/ \
  -v /etc/letsencrypt/:/certs \
  -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/live/cdaringe.com/fullchain.pem \
  -e REGISTRY_HTTP_TLS_KEY=/certs/live/cdaringe.com/privkey.pem \
  registry:2
```
