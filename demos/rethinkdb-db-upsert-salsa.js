// to first create...
r.table('salsademo').insert({
  id: 'hfd', // <== would generated random id if not specified
  name: 'Hot Firey Death',
  manufacturer: 'Organic Salsas Unlimited, Inc',
  firstImpression: `Gazooks!`,
  uniqueness: `Only salsa to melt my face.`,
  flavors: `...`,
  recommendation: `Spoon a day keeps the doctor away.`,
  experience: `...`,
  price_per_unit: 5.00,
  price_per_oz: 0.89,
  texture: 2,
  flavor: 2,
  hotness: 5,
  presentation: 3,
  images: {
    primary: 'organic-hot-firey-death.jpg',
    secondary: ['organic-hot-firey-death-2.jpg']
  },
})

// if we want to update...
r.table('salsademo').update({
  id: 'hfd', // <== would generated random id if not specified
  name: 'Hot Firey Death',
  manufacturer: 'Organic Salsas Unlimited, Inc',
  firstImpression: `Gazooks!`,
  uniqueness: `Only salsa to melt my face.`,
  flavors: `...`,
  recommendation: `Spoon a day keeps the doctor away.`,
  experience: `...`,
  price_per_unit: 5.00,
  price_per_oz: 0.89,
  texture: 2,
  flavor: 2,
  hotness: 5,
  presentation: 3,
  images: {
    primary: 'organic-hot-firey-death.jpg',
    secondary: ['organic-hot-firey-death-2.jpg']
  },
})
