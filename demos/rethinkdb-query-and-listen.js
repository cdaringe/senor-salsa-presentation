// let's sniff for no-so-spicy salsas
// r.table('salsademo')
//   .changes()
//   .filter(
//   	r.row('new_val')('hotness').lt(2)
// 	)

'use strict'

const r = require('rethinkdb')
let connection = null
r.connect({ host: 'localhost', port: 28015 }, function(err, conn) {
  if (err) throw err
  connection = conn
  r.table('salsademo')
  .changes()
  .filter(r.row('new_val')('hotness').lt(2))
  .run(connection, (err, cursor) => {
    if (err) throw err
    cursor.each((err, row) => {
      if (err) throw err
      console.log('NOT-SO-SPICY-SALSA-DETECTED!\n\n',JSON.stringify(row, null, 2))
    });
  })
  console.log('LISTENING FOR NOT-SO-SPICY-SALSAS!')
})


// what if we are doing a complex query across tables?
// no problem. https://www.rethinkdb.com/docs/table-joins/
